package jp.alhinc.masubuchi_yuya.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.regex.Pattern;

public class CalculateSales {
	public static void main(String[] args) {

		Map<String, String> map1 = new HashMap<String, String>();
		Map<String, Long> map3 = new HashMap<String, Long>();

		if (!(fileReader(args, "branch.lst",map1))) {
			return;
		}

		long totalprice = 0;
		BufferedReader br = null;

		try {
			File file = new File(args[0], "branch.lst");

			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);

			String line;
			while ((line = br.readLine()) != null) {

				String[] bank = line.split(",");

				map3.put(bank[0], totalprice);

			}

		} catch (IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return;
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					System.out.println("closeできませんでした");
					return;
				}
			}

		}

		File dir = new File(args[0]);
		File[] list = dir.listFiles();

		List<Integer> numList = new ArrayList<Integer>();
		try {

			for (int i = 0; i < list.length ; i++) {

				if (list[i].getName().matches("[0-9]{8}.rcd")) {
					if (list[i].isFile()) {

						String[] num = list[i].getName().split(Pattern.quote("."));

						int nums = Integer.parseInt(num[0]);


						numList.add(nums);
					}

				}
			}


			for (int a = 1; a < numList.size() ; a++) {
						if (numList.get(a) - numList.get(a-1) != 1) {

							System.out.println("売上ファイル名が連番になっていません");
							return;

						}

			}




			for (int i = 0; i < list.length; i++) {
				if (list[i].getName().matches("[0-9]{8}.rcd")) {

					FileReader fr = new FileReader(list[i]);
					br = new BufferedReader(fr);

					String line;
					Map<String, String> map2 = new HashMap<String, String>();
					List<String> lineList = new ArrayList<String>();
					while ((line = br.readLine()) != null) {
						lineList.add(line);

					}

					if (lineList.size() != 2) {
						System.out.println(list[i].getName() + "のフォーマットが不正です");
						fr.close();
						return;
					}

					map2.put(lineList.get(0), lineList.get(1));

					long salesprice = Long.parseLong(lineList.get(1));

					if (!(map3.containsKey(lineList.get(0)))) {
						System.out.println(list[i].getName() + "の支店コードが不正です");
						fr.close();
						return;

					}

					for (Map.Entry<String, Long> check : map3.entrySet()) {

						if (check.getKey().equals(lineList.get(0))) {

							Long price = Long.parseLong(check.getValue().toString());

							price += salesprice;

							if (price.toString().matches("[0-9]{11,}")) {
								System.out.println("合計金額が10桁を超えました");
								fr.close();
								return;
							}

							map3.put(check.getKey(), price);

						}

					}

				}

			}

		} catch (IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return;
		} catch (NumberFormatException e) {
			System.out.println("予期せぬエラーが発生しました");
			return;
		}

		finally {
			if (br != null) {
				try {

					br.close();
				} catch (IOException e) {
					System.out.println("closeできませんでした");
					return;
				}
			}

		}

		if (!(printWriter(args[0], "branch.out", map3, map1))) {
			return;
		}

	}

	public static boolean printWriter(String args, String filepoint, Map<String, Long> map3, Map<String, String> map1) {

		String pwlist = null;

		try {
			File file = new File(args, filepoint);

			FileWriter fi = new FileWriter(file);

			PrintWriter pw = new PrintWriter(new BufferedWriter(fi));

			for (Entry<String, String> check : map1.entrySet()) {

				String lasts = check.getKey() + "," + check.getValue();

				for (Map.Entry<String, Long> check2 : map3.entrySet()) {
					if (check2.getKey().equals(check.getKey())) {

						pwlist = lasts + "," + check2.getValue();

					}

				}

				pw.println(pwlist);

			}
			pw.close();
			return true;

		} catch (IOException e) {

			System.out.println("予期せぬエラーが発生しました");
			return false;
		}

	}

	public static boolean fileReader(String[] args, String code,Map<String, String> map1) {
		if (args.length >= 2) {
			System.out.println("予期せぬエラーが発生しました");
			return false;
		}
		BufferedReader br = null;

		try {

			File file = new File(args[0], code);
			if (!file.exists()) {
				System.out.println("支店定義ファイルが存在しません");
				return false;
			}

			FileReader fr = new FileReader(file);

			br = new BufferedReader(fr);

			String line;

			while ((line = br.readLine()) != null) {
				if (!line.matches("[0-9]{3},.*")) {
					System.out.println("支店定義ファイルのフォーマットが不正です");
					return false;
				}

				String[] bank = line.split(",");
				if (bank.length != 2) {
					System.out.println("支店定義ファイルのフォーマットが不正です");
					fr.close();
					return false;
				}

				map1.put(bank[0], bank[1]);

			}
			return true;

		} catch (IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return false;
		} catch (ArrayIndexOutOfBoundsException e) {
			System.out.println("予期せぬエラーが発生しました");
			return false;
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					System.out.println("closeできませんでした");
					return false;
				}
			}

		}

	}

}
